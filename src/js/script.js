var dbt = "2Kxq5I7YEqEAAAAAAAAJ99xXKmnf4wv4nfg51rb73RRP9yPmV_xJ1tlpuRqEQ2sp"
var dbx = new Dropbox({ accessToken: dbt });

var $ = function(id) {
  return document.getElementById(id);
};

//var searchUrl = "https://www.google.com/#hl=en&q={Q}";
//var searchUrl = "https://duckduckgo.com/?q={Q}"
var searchUrl = "https://www.startpage.com/do/search?query={Q}"
var searchInput = $("search_bar");
var ignoredKeys = [9,13,16,17,18,19,20,27,33,34,35,36,37,38,39,40,45,46,91,92,93,112,113,114,115,116,117,118,119,120,121,122,123,144,145];

function init() {
  searchInput.focus()
  wp_init()
  menu_init()
  quote_init()
}

function readAsBase64(blob, cb) {
  var r = new FileReader();
  r.onload = function() {
    var data = r.result;
    cb( data.substr(data.indexOf(",") + 1) );
  };
  r.readAsDataURL(blob);
}

function readFile(blob, cb, save){
  let r = new FileReader();
  r.addEventListener("loadend", function() {
    save(r.result)
    cb(r.result);
  });
  r.readAsText(blob);
}

function wp_init(){
  var ck = getCookie("wplist")

  if (ck != "")
    select_random_file(JSON.parse(ck))
  else 
    db_init()

}

function menu_init(){
  var ck = getCookie("menulist")

  if (ck != "")
    json_list(JSON.parse(ck))
  else 
    get_json()
}

function quote_init(){
  var ck = localStorage.getItem("quotelist")

  if (ck){
    select_random_quote(JSON.parse(ck))
  }
  else 
    get_quotes()

}

function get_json(){
  dbx.filesDownload({path: "/nt-p.json"})
    .then(function(res) {
      readFile(res.fileBlob, json_list, function(r){
        setCookie("menulist", JSON.stringify(r), 24)
      })
    })
    .catch(function(err) {
    });
}

function get_quotes(){
  dbx.filesDownload({path: "/quotes.json"})
    .then(function(res) {
      readFile(res.fileBlob, select_random_quote, function(r){
        localStorage.setItem("quotelist", JSON.stringify(r))
      })
    })
    .catch(function(err) {
    });
}

function select_random_quote(res){
  let quotes = JSON.parse(res)
  var i = Math.floor(Math.random() * quotes.length)
  let q = $("quote")
  q.innerHTML = quotes[i]
}

function json_list(res){
  let menu_list = JSON.parse(res)
  let parent = $("nav-parent")

  menu_list.forEach(function(menu){

    let aa = document.createElement("a")
    aa.setAttribute("href", menu.url)
    aa.setAttribute("class", "col-xs-12 col-sm-6")

    let ul = document.createElement("ul")
    ul.setAttribute("class", "list-group list-grey")

    let li = document.createElement("li")
    li.setAttribute("class", "list-group-item pointer hcenter")
    
    let strong = document.createElement("strong")
    strong.innerHTML = menu.name;

    li.appendChild(strong)
    ul.appendChild(li)
    aa.appendChild(ul)
    parent.appendChild(aa)
  })

}

function db_init(){
  dbx.filesListFolder({path: ""})
    .then(function(res) {

      var imgs = res.entries.filter(function(entry) {
        return (entry.name != "nt-p.json" && entry.name != "quotes.json")
      })
      setCookie("wplist", JSON.stringify(imgs), 24)
      console.log(imgs.length)
      select_random_file(imgs)
    })
    .catch(function(err) {
    });
}

function select_random_file(files){
  var i = Math.floor(Math.random() * files.length)
  var id = files[i].id
  var im = localStorage.getItem("wp-"+id)

  if (im == null)
    db_getfile(id)
  else
    setbackground(im)

}

function db_getfile(id){
  dbx.filesDownload({path: id})
    .then(function(res) {
      blob_to_base64(id, res.fileBlob)
    })
    .catch(function(err) {
    });
}

function blob_to_base64(id, blob){
  readAsBase64(blob, function(base64){
    localStorage.setItem("wp-"+id, base64)
    setbackground(base64)
  })
}

function setbackground(base64){
  var b64_pre = "url(data:image/png;base64,";
  var b64_pos = ")";
  var bg = b64_pre + base64 + b64_pos
  var body = document.getElementsByTagName("body")[0];
  body.style.background = bg
}

function handleQuery(event, query) {
  var key = event.keyCode || event.which;
  if(query !== "") {
    var qlist;
    if(key == 13 && event.ctrlKey){
      window.location.replace("https://" + query + ".com")
    }
    else if(key == 13 && event.shiftKey){
      window.location.replace("https://" + query + ".de")
    }
    else if (key === 13) {
      qList = query.split(" ");
      window.location = searchUrl.replace("{Q}", encodeURIComponent(query));
    }
  }
  if (key === 27) {
    searchInput.blur();
  }
}

function handleKeydown(event) {
  if (searchInput === document.activeElement || 
     ignoredKeys.includes(event.keyCode))
    return;

  searchInput.focus();
}

function setCookie(cname, cvalue, exhours) {
  var d = new Date();
  d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}