var gulp = require('gulp'),
  rename = require('gulp-rename'),
  inlinesource = require('gulp-inline-source'),
  cleancss = require('gulp-clean-css'),
  minify = require('gulp-minify'),
  imagemin = require('gulp-imagemin'),
  pug = require('gulp-pug');

gulp.task('pug', function buildHTML() {
  return gulp.src('src/index.pug')
    .pipe(pug({
      verbose: true
    }))
    .pipe(gulp.dest('src/'));
});

gulp.task('inline', function () {
  return gulp.src('src/index.html')
    .pipe(inlinesource())
    .pipe(gulp.dest('public/'));
});

gulp.task('minify-css', () => {
  return gulp.src('src/css/*.css')
    .pipe(cleancss({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/css'));
});

gulp.task('minify-js', function() {
  gulp.src('src/js/*')
    .pipe(minify({
        ext:{
            src:'_.js',
            min:'.js'
        }}))
    .pipe(gulp.dest('public/js'))
});

gulp.task('minify-img', () =>
  gulp.src('src/css/*')
    .pipe(imagemin())
    .pipe(gulp.dest('public/css'))
);

gulp.task('minify', function () {
  gulp.start('minify-css');
  gulp.start('minify-js');
  //gulp.start('minify-img');
});

gulp.task('build', function () {
  gulp.start('minify');
  gulp.start('inline');
});

gulp.task('default', ['build']);